### 1. Informace a jejich význam

- pojem informatika, informace
- obory informatiky
- uchovávání informací
- kódování informací, dvojková a šestnáctková soustava
- šifrování
- analogová a digitální zařízení
- bezeztrátová a ztrátová komprese dat
- elektronický podpis, bezpečnost – spam, hoax, sociální inženýrství

### 2. Hardware – součásti počítače, vstupní a výstupní zařízení

- vysvětlení jejich funkce z hlediska počítačové sestavy
- John Von Neumannovo schéma
- charakteristické parametry, typické hodnoty a vliv na celkový výkon počítače
- datová úložiště a záznamová média

### 3. Software, Škodlivý software

- autorská práva, druhy SW, licence
- formáty dokumentů
- operační systémy – druhy a jejich charakteristika
- pojem, projevy, části počítačového viru, rozdělení virů
- viry, trojské koně, červi
- antivirový SW

### 4. Počítačové sítě

- význam počítačových sítí
- rozdělení dle rozsahu
- architektura sítě
- topologie sítě
- aktivní a pasivní prvky
- MAC adresa, IP adresa, DNS

### 5. Rastrová grafika

- princip
- souborové formáty
- programy pro editaci
- použití
- technické vlastnosti (rozlišení, definice barev, barevné prostory, barevná hloubka, datová velikost)
- komprese
- srovnání vektorové a rastrové grafiky (výhody, nevýhody)
- praktická úloha

### 6. Vektorová grafika

- základní princip
- souřadnicová soustava
- jednoduché objekty, předdefinované tvary, křivky
- Bézierova křivka
- vlastnosti
- programy pro tvorbu
- použití vektorové grafiky
- export do rastrového formátu, konverze do PDF
- srovnání vektorové a rastrové grafiky (výhody, nevýhody)
- praktická úloha

### 7. Tvorba www stránek - HTML

- praktická úloha

### 8. Tvorba www stránek – CSS

- praktická úloha

### 9. Textové editory - rozsáhlé dokumenty

- generování obsahu
- seznamy obrázků, seznam literatury, rejstřík, komentáře
- použití stylů, titulní strana
- praktická úloha

### 10. Textové editory

- hromadná korespondence
- typografie
- praktická úloha

### 11. Tabulkové kalkulátory

- praktická úloha (vzorce, adresování, suma, průměr, formát buňky)

### 12. Tabulkové kalkulátory

- praktická úloha (podmínky, matematické, funkce, grafy)

### 13. Databázové programy

- pojmy databáze, tabulka, záznam, pole
- datový typ a vlastnosti
- index a jeho význam
- primární klíč, vztah mezi tabulkami, cizí klíč, referenční integrita
- význam databází pro praxi a jejich propojení v informačním systému, transakční zpracování

### 14. Databázové programy

- praktická úloha

### 15. Prezentační programy

- zásady prezentace
- zpracování počítačové prezentace
- šablony, animace, přechody snímků
- automatické přehrávání, časování, odkazy, převod do PDF
- praktická úloha

### 16. Algoritmizace a programování

- algoritmus, vlastnosti
- možnosti zápisu algoritmu
- algoritmizace a její části
- proměnná, identifikátor, datový typ, deklarace, syntaxe, strojový kód
- rozdělení programovacích jazyků
- vývojový diagram

### 17. Programování - teorie

- Základní pojmy
- Jednoduché datové typy
- Definice proměnných, přiřazení
- Vstup a výstup
- Řídící struktury

### 18. Programování

- praktická úloha – řídící struktury

### 19. Programování

- praktická úloha – cykly

### 20. 3D modelování

- Autodesk Inventor
- modelování součástí
- výkresy
- praktická úloha

### 21. 3D modelování

- Autodesk Inventor
- sestavy
- prezentace
- praktická úloha

### 22. Programování PHP

- datové typy
- propojení html a php
- řídící struktury
- cykly
- praktická úloha

### 23. Databáze v MySql

- propojení PHP a MySQL
- syntaxe SQL
- použití výběrových dotazů s využitím SQL
- praktická úloha

### 24. Rozhraní počítače

- pojem rozhraní počítače
- rozdělení
- druhy a charakteristika jednotlivých rozhraní
- praktická ukázka

### 25. Základy robotiky

- programovací prostředí
- práce s jednotlivými typy senzorů
- ovládání pohybu
- praktická úloha
