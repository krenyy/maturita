> [!info] Hlavní rysy
>
> - vznikl ve Francii ve 2. polovině 17. století až ke konci 18. století
> - důraz na:
>   - rozum
>   - racionalismus
>   - příklon k antickým vzorům
>   - jasnost
>   - harmonii
>   - řád
>   - uměřenost
>   - pravidelnost
> - inspirace Aritotelovou Poetikou:
>   - umění je napodobení přírody - statické, její podstata se nemění, umění zachycuje stálé, obecné, nadčasové
>   - antika chápána jako něco minulého, z čeho máme vycházet, nikoli se k tomu vracet
> - požadavky na obsah a formu díla:
>   - inspirace antickým uměním
>   - žánry vysoké (epos, óda, tragédie)
>   - žánry nízké (bajka, satira, komedie, píseň)
>   - vyhraňování literárních typů (hrdina, lakomec, ctnostná žena)
>   - zákon tří jednot (místa, času, děje)
