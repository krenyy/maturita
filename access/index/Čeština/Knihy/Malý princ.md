> [!important]- Na potítku
>
> <iframe allowfullscreen frameborder="0" width="640" height="360" src="https://www.youtube.com/embed/Rv2bfXHpdQI"></iframe>

### Autor

- [[Antoine de Saint-Exupéry]]

### Literární

- **druh:**
  - epika
- **forma:**
  - próza
- **žánr:**
  - filozofická pohádka
- **směr:**
  - meziválečná literatura, francouzský realismus 1. poloviny 20. století

### Obsah

- Autorovo letadlo mělo poruchu a on přistál v poušti. Setkal se s malým
  princem, který přišel z jiné planety, a spřátelili se spolu. Malý princ po něm
  chtěl, aby mu nakreslil beránka. Byl spokojen, když mu letec nakreslil
  bedničku. Představoval si, že je beránek v bedničce. Princ mu vyprávěl svoje
  vzpomínky. Odešel ze své malinké planety, na které jsou tři sopky a jediná
  květina, protože se svou kytkou měl problémy.
- Navštívil sedm planet. Na první planetě byl král, jenž žil sám a chtěl někomu
  vládnout. Myslel si, že vládne celému vesmíru. Viděl v princovi svého
  poddaného. Na druhé planetě žil domýšlivec, který v každém viděl svého
  obdivovatele. Na třetí planetě žil pijan. Na čtvrté byznysmen, který počítal
  hvězdy. Myslel si, že mu patří. Na páté planetě žil lampář, který rozsvěcoval
  a zhasínal lampu. Ale jeho planeta se točila tak rychle, že ji rozsvěcoval
  nebo zhasínal každou minutu. Na šesté planetě žil zeměpisec, jenž považoval
  svoji vědu za nejdůležitější, pouze zapisoval do knih a připadal si příliš
  důležitý na to, aby počítal města, řeky a hory. Čeká na badatele, kteří mu
  podají zprávu o okolí.
- Sedmá planeta, jíž navštívil, byla Země. Setkal se tady s hadem, růžemi a
  liškou, která se stala jeho přítelkyní, protože si ji ochočil. Díky lišce
  pochopil, co je to přátelství, a objevil znovu svoji lásku ke své jediné růži,
  která je na jeho planetě. Setkal se s výhybkářem, který mu řekl, že lidé
  nevědí, proč jezdí sem a tam vlakem, a navíc tak rychle. Potkal také
  obchodníka, který vymýšlí pilulky, díky nimž člověk nepotřebuje pít, a tím si
  spoří čas.
- Potom se setkal s letcem, který se stal jeho přítelem. Letec opravil své
  letadlo a našel s malým princem studnu s vodou. Duše malého prince opustila
  Zemi díky hadu, který ho uštknul. Malý princ nechal na Zemi svoje tělo a
  vrátil se ke své květině. Letec na něj bude celý život s láskou vzpomínat,
  obzvlášť, když se podívá na hvězdy.

### Členění

- kapitoly

### Kompozice

- chronologická když vypráví pilot
- retrospektivní když vypráví malý princ své zážitky z ostatních planetek

### Téma

- střet dětského světa se světem dospělých
- Autor se snaží poukázát na fakt, že děti a dospělí si spolu občas nerozumí a
  neshodnou se v chápání světa. Dále poukazuje na dětskou fantazii, mysl a
  přátelství.

### Motivy

- přátelství
- láska
- alkoholismus
- domýšlivost

### Postavy

- **Malý princ**
  - Přátelský milý chlapec, který se díky své tvrdohlavosti nikdy nevzdá
    odpovědi na svou otázku. V každém se vždycky pokouší najít to nejlepší. Dívá
    se na svět optimisticky a kvůli tomu občas nechápe, proč dospělí řeší
    některé problémy, které se mu zdají hloupé a nepodstatné. Proto většinou
    tvrdí, že dospělí jsou zvláštní.
- **Pilot**
  - Zároveň autor. Chápavý, přátelský a milý. Snažil se porozumět malému princi,
    i když někdy marně. Uvědomil si jaký býval, když on sám byl dítětem. S malým
    princem byl dobrý přítel. Ukazuje, jak by se měli lidi pokoušet naslouchat
    ostatním.
- **Květina**
  - domýšlivá
- **Král**
  - panovačný, myslí si, že je rozumný, potřebuje si dokazovat svou autoritu
- **Domýšlivec**
  - samolibý
- **Pijan**
  - nešťastný, osamělý
- **Byznysmen**
  - zaměstnaný, vážný, nebaví se o hloupostech, nemá čas, zaneprázdněný, přesný
- **Lampář**
  - pracovitý, věrný příkazu
- **Zeměpisec**
  - myslí si, že je nejdůležitější na světě
- **Výhybkář, obchodník**
  - postiženi spěchem a hektickým způsobem života na Zemi, který nemá smysl
- **Liška**
  - znuděná, potřebuje přítele
- **Had**

### Časoprostor

- na Sahaře, na planetě B612, Zemi a planetách okolo
- někdy v 1. polovině 20. století

### Vypravěcí způsoby

- vyprávění pilota - ich-forma
- popis a vyprávění Malého prince - er-forma

### Jazyk

- dialogy
- hyperbola
- metafora
- personifikace
- spisovná čeština
- vnitřní monology
