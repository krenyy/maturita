### Autor

- [[Michail Šolochov]]

### Literární

- **druh:**
  - epika
- **forma:**
  - próza
- **žánr:**
  - novela
- **směr:**
  - ruský realismus 2. poloviny 20. století, poválečná literatura

### Obsah

- Osud prostého člověka Andreje Sokolova. Autor v létě 1946 jede ruskou krajinou
  a má problémy se zničenými komunikacemi. Dostane se do jedné vesnice, kde
  potká Sokolova s malým chlapcem. Muži si zakouří a Sokolov vypráví svůj
  příběh.
- Sokolov byl řidič ve Voroněži, který se zamiloval a nakonec oženil a žili
  spokojeně s manželkou. Manželka byla velmi klidná a rozumná žena, která pro
  něj měla pochopení a nikdy mu nic nevyčítala a on se snažil co nejlépe se
  postarat o rodinu. Časem se jim narodil syn a potom dvě dcerky.
- Jakmile Sovětský svaz vstoupí do války, přes protest manželky narukuje a
  zakrátko je zajat a deportován přes Polsko do Německa do koncentračního
  tábora. Líčí otřesné podmínky života v lágru a nelidské chování německých
  bachařů. Svým vojenským vystupováním a prostotou ruského človíčka si získá
  obdiv německého důstojníka Müllera, zachrání si život a pracuje v dolech do
  roku 1944. Potom dostane za úkol dělat řidiče tlustému a věčně opilému
  německému majorovi. Využije příležitosti a i s majorem se dostane k Rudé
  armádě.
- Po ošetření v nemocnici odjede zpět do rodného kraje, kde se dozvídá, že na
  počátku války manželka i dcerky zahynuly. Sokolov nedokáže být doma sám, vrací
  se do boje a opět na něj čeká tragická zpráva, jeho syn - hrdina - padl při
  dobývání Berlína v posledních dnech války.
- Válka tedy vítězně skončila, ale Sokolov se nemá z čeho radovat, protože
  všechno ztratil, zůstal sám. Nedokáže bydlet ve svém prázdném domě, jede k
  příteli do Urjupinska a opět pracuje jako řidič. Dovídá se, že malý chlapec je
  sirotkem, a protože si malý na svého otce již nepamatuje, vydává se Sokolov za
  chlapcova otce. Tím dostane jeho život nový smysl, má povinnost se o chlapce
  postarat. Spolu s chlapcem se vydávají do Kaškarské oblasti za prací a cestou
  potkali vypravěče.

### Členění

-

### Kompozice

- Chronologická: začátek (Andrej přijde setká se s posluchačem)
- Retrospektivní: hlavní část (Andrej vypráví příběh)

### Téma

- příběh Andreje Sokolova před, během a po II.sv.válce (život)

### Motivy

-

### Postavy

- **Andrej Sokolov**
  - příběh vypráví o tragickém osudu tohoto muže v příběhu se jeví jako slušný a
    poctivě pracující člověk, kterého poznamenala válka. Sokolov je rozvážný a
    odvážný člověk. Jediným jeho neduhem je sklenička vodky, kterou si denně
    dopřává (sám přizná, že jde o zlozvyk). S obdivuhodnou statečností se staví
    k ranám a zkušenostem, které mu život připravil. I přes rány osudu má život
    rád.
- **Ivan (Váňušek)**
  - sirotek, kterého si Andrej osvojil
- **Poutník**
  - je to sám autor, který je v příběhu jen proto, aby poslouchal Andrejovo
    vyprávění. Neovlivňuje nijak děj.

### Časoprostor

- Horní Don, poválečné jaro 1946

### Vypravěcí způsoby

-

### Jazyk

- Jednoduchý a dobře srozumitelný jazyk
- Občas hovorové výrazy
- Personifikace (přišel liják)
- Hyperbola (všichni jsme promokli skrz na skrz)
- Přirovnání
- Frazeologismus (dělaly se mi mžitky před očima)

### Syntax

-
