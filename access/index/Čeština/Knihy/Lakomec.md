> [!important]- Na potítku
>
> <iframe allowfullscreen frameborder="0" width="640" height="360" src="https://www.youtube.com/embed/YnEfTjxKnRU"></iframe>

### Analýza uměleckého textu

#### 1. část

> [!info]+ Zasazení výňatku do kontextu díla
>
> > [!example]- Obsah
> > Harpagon je bohatý lakomec. Každého podezírá z krádeže, včetně svých dětí (syna a dcery). Jeho syn Kleantes je zamilován do chudé dívky Mariany, která jeho lásku opětuje, a chce otce požádat o svolení ke sňatku. Otec syna překvapí tím, že si Marianu chce vzít on sám. Kleanta by rád oženil s bohatou vdovou. Dceru Elišku, která je zamilovaná do Valéra, chce provdat za stárnoucího šlechtice Anselma. Sňatky v domě organizuje důvtipná dohazovačka Frosina, která je přítomna také u námluv H. s Marianou v H. domě, kde se Mariana dozví, že její milý Kleantes je H. syn. Kleantův sluha Čipera zjistí, kde Harpagon schovává většinu svých peněz, je v truhle, kt. H. zakopal na zahradě a pravidelně chodí kontrolovat, zda mu ji někdo neodcizil. Čipera se o pokladu dozví a uzme jej, aby tak Kleantovi pomohl. H. začne zběsile pátrat a podezřívat každého ve svém okolí, zavolá si a podplatí komisaře, aby zloděje co nejrychleji vypátral. Při vyslýchání Valéra se dozví, že se zasnoubil s jeho dcerou. V soudní síni Valér prozradí, že jeho otcem je italský šlechtic, z čehož vyplyne, že je to Anselm. Dále se dozvídáme, že Mariana je jeho sestra a Anselmova dcera. Kleantes svému otci navrhne, že mu peníze vrátí, když mu dovolí vzít si Marianu za ženu. Harpagon obětuje vše, jen aby získal své peníze zpět. Když poznal pravý původ Mariany a Valéra, pochopil, že vdavky jej nebudou stát ani tolar a své dceři taktéž dávat žádné věno nemusí, s oběma svatbami souhlasí. Valér se ožení s Eliškou a Kleantes si vezme Marianu. Příběh tedy končí šťastně, Harpagon nadále zůstává sám se svými penězi.

> [!info]+ Téma
>
> - chorobná touha po penězích
> - lakota
> - posedlost a zaslepenost penězi vedoucí až k šílenství

> [!info]+ Motivy
>
> - hádka
> - krádež
> - lakota
> - láska
> - nalezení
> - peníze
> - přátelství
> - rodina
> - vztahy

> [!info]+ Časoprostor
>
> - 1670, Paříž, Francie

> [!info]+ Kompoziční výstavba
>
> - chronologická
> - 5 dějství
> - dodržení tří jednot

> [!info]+ Literární
>
> > [!example] Druh
> >
> > - drama
>
> > [!example] Žánr
> >
> > - satirická komedie

#### 2. část

> [!info]+ Vypravěč / lyrický subjekt
>
> - není

> [!info]+ Postavy
>
> > [!tip]- Harpagon
> >
> > - hlavní postava
> > - lakomý vdovec, který se zajímá jen o své peníze
> > - je velmi chamtivý a bezcitný
> > - pro peníze je schopen obětovat absolutně všechno - jsou pro něj důležitější než jeho rodina
> > - bohatne půjčováním peněz s vysokým úrokem (lichva)
> > - nenapravitelný sobec, jenž neuznává city druhých
> > - chce výhodně provdat a oženit své děti
> > - sám touží po mladé dívce Marianě
>
> > [!tip]- Kleant
> >
> > - syn Harpagona
> > - mladý muž dobrých společenských způsobů, velmi kultivovaný
> > - zamilovaný do Mariany a chce si ji vzít
>
> > [!tip]- Eliška
> >
> > - dcera Harpagona
> > - je zamilovaná do Valéra
> > - bojuje za své štěstí a nechce se provdat za muže, kterého jí vybral otec a kterého nemiluje
> > - věří v pravou lásku
>
> > [!tip]- Mariana
> >
> > - velmi krásná dívka, miluje Kleanta a nešťastně přihlíží Harpagonovu dvoření
> > - skromná a plachá, působí velmi křehce
> > - vede tichý život po boku své nemocné matky
>
> > [!tip]- Valér
> >
> > - zamilovaný do Elišky, pracuje v domě Harpagona jako správce, aby byl Elišce nablízku
> > - snaží se co nejvíce se zalíbit Harpagonovi, proto se stává úlisným slídilem
> > - pronásleduje své podřízené mnohdy ještě hůře než Harpagon, donáší na služebnictvo
> > - chce se před svým pánem ukázat v co nejlepším světle
>
> > [!tip]- Anselm
> >
> > - navenek bohatý, starý muž, štědrý a dobrosrdečný šlechtic - otec Mariany a Valéra
> > - později se ukáže, že jeho pravé jméno je Tomas d'Alburci
> > - svým vstupem do hry završí děj dobrým koncem
>
> > [!tip]- Frosina
> >
> > - chytrá intrikářka, dohazovačka
> > - jako jediná zná poměry v celé rodině
>
> > [!tip]- Štika
> >
> > - sluha Kleanta
> > - je si vědom Harpagonových vlastností
>
> > [!tip]- Jakub
> >
> > - Harpagonův kuchař, kočí a sluha
> > - podlý a pomstychtivý
>
> > [!quote] Policejní komisař
>
> > [!quote] Písař policejního komisaře
>
> > [!quote]- Harpagonovo služebnictvo
> >
> > - Bumbal
> > - Treska
> > - mistr Šimon
> > - panna Klaudie

> [!info]+ Vyprávěcí formy
>
> - ich forma
> - psáno formou prózy

> [!info]+ Typy promluv
>
> - časté nevlastní přímé řeči (v promluvách postav)
> - nepřímá řeč ve scénických poznámkách

#### 3. část

> [!info]+ Jazykové prostředky
>
> - spisovný jazyk (17. století)
> - hovorové a nespisovné výrazy (služebnictvo)
> - archaismy
> - občasné vulgarismy
> - citoslovce
> - satira

> [!info]+ Tropy
>
> - metafora
> - ironie

> [!info]+ Figury
>
> - epizeuxis
> - hyperbola (nadsázka)
> - eufemismus (přikrášlení)
> - dysfemismus (zhrubění)

### Literárně-historický kontext díla

![[Molière]]
