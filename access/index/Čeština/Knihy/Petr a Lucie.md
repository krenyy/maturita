> [!important]- Na potítku
>
> <iframe allowfullscreen frameborder="0" width="640" height="360" src="https://www.youtube.com/embed/v6OH8A4aAgs"></iframe>

### Autor

- [[Romain Rolland]]

### Literární

- **druh:**
  - epika
- ## **forma:**
- **žánr:**
  - milostná novela
- **směr:**
  - 1\. světová válka ve světové literatuře
  - konec 19. století, 1. polovina 20. století

### Obsah

-

### Členění

-

### Kompozice

-

### Téma

-

### Motivy

-

### Postavy

- ## **Postava**

### Časoprostor

-

### Vypravěcí způsoby

-

### Jazyk

-

### Syntax

-
