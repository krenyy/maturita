> [!important]- Na potítku
>
> <iframe allowfullscreen frameborder="0" width="640" height="360" src="https://www.youtube.com/embed/BaAfJCagzh0"></iframe>

### Autor

- [[Karel Hynek Mácha]]

### Literární

- **druh:**
  - lyriko-epika
- **forma:**
  - poezie
- **žánr:**
  - básnická povídka
- **směr:**
  - romantismus

### Obsah

- _1. zpěv_ Báseň začíná oslavou máje, popisem krásné májové přírody. Jarmila
  čeká na svého milého na břehu jezera. Místo něj však připlouvá jeho přítel se
  zprávou, že Vílém má být druhého dne popraven za vraždu Jarmilina svůdce.
  Jarmila skoří do jezera a utopí se.
- _2. zpěv_ Vilém ve vězení přemýšlí, přesvědčuje se, že není vinen. Dozvídá se,
  že zabil svého otce, který ho kdysi vyhodil z domu, a kvůli němu se stal
  vůdcem loupežníků. Počítá padající kapky, jako by měřil čas.
- _1. intermezzo_ Na popravišti čeká sbor duchů na Viléma
- _3. zpěv_ Vilém se loučí s přírodou, kterou už nikdy neuvidí. Obává se, co
  bude po smrti. Davy lidí vybíhají na louky, aby si ho prohlédly. Vilém je
  popraven a jeho tělo je upleteno do kola.
- _2. intermezzo_ Vilémovi spoluloupežníci sedí kolem ohně a mlčí. Jsou
  nešťastní ze ztráty jejich vůdce.
- _4. zpěv_ Do vesnice se vrací sám autor po sedmi letech poslední dech v roce.
  V hospodě se povídá příběh Viléma a Jarmily. Opět se vrací prvního máje ke
  kolu. Děj končí ztotožnením se Máchy s dějem.

- Ve čtyřech zpěvech, odehrávajících se v květnu ve třech denních dobách (večer,
  noc, ráno), a dvou intermezzech vylíčil básník tragédii tří lidí, která
  přerostla v tragédii obecně lidskou. Prostý příběh založený na skutečné
  události se prolína s podmanivě zobrazenou krásou přírody. Myšlenková hloubka
  Máje je podpořena mistrovskou formou.
- Vilém zavraždí svůdce své milé Jarmily. Až poté zjistí, že zabil svého
  vlastního otce, kterým byl kdysy zavřen, kvůli čemuž se z něj stal obávaný
  vůdce loupežníků. Je vězněn a čeká v cele na popravu - přemýšlí o životě a
  smrti, vině a trestu, o nicotě a marnosti, o své popravě a osudné náhodě,
  která ho do vězení přivedla. Tato část přináší jeden z nejsilnějších momentů
  celé básně, kdy Vilém v posledních chvílích před smrtí medituje a přemýšlí o
  životě, hodnotách, vlasti, i náboženství. V této těžké situaci jej děsí
  představa, že po smrti už není nic.
- Když se Jarmila dozví o smrti svého milého, tak skončí svůj život skokem do
  jezera. Na místo popravy přichází po letech vypravěč básně, medituje nad
  lebkou a kostmi Viléma, a v posledním verši (Hynku, Viléme, Jarmilo) se
  ztotožní s jejich tragédií.

### Členění

- 4 dějství/zpěvy, 2 intermezza

### Kompozice

- chronologická

### Téma

- Nešťastný životní osud Viléma a Jarmily
- Zobrazení májové přírody

### Motivy

- myšlenky lepší budoucnosti
- myšlenky smrti
- májová příroda
- otcovražda
- sebelítost
- smrt
- smutek
- tragická láska
- trest smrti

### Postavy

- **Vilém**
  - Když byl Vilém malý, vyhnal ho jeho otec z domu a měl tak nelehké dětství;
    přidal se k loupežníkům a po čase se stal jejich vůdcem; byl pohledný,
    protože by asi jinak nesvedl Jarmilu; žárlil na svého otce, že měl s
    Jarmilou taky vztah a nakonec ho zabil; díky svému nepřemýšlivému jednání
    byl nevázaný, nespoutaný i pomstychtivý a prchlivý; dalo by se říci, že byl
    i statečný a šikovný, protože spáchat vraždu nebylo asi úplně jednoduché; v
    nitru dobrý člověk, lidé ho měli rádi; až ve vězení začíná trochu uvažovat a
    přemýšlet; cítí vinu, bohužel pozdě, a jeho život je ukončen na popravišti
- **Jarmila**
  - Půvabná a pohledná mladá dívka, která miluje Viléma, ale je svedena i jeho
    otcem; snadno ovlivnitelná (což dokazuje vztah s otcem), má nepevný
    charakter a zkratovité jednání, o čemž svědčí nepromyšlená a okamžitá
    sebevražda; ale je trpělivá, protože stále čekala na Viléma, i když slunce
    již dávno zapadlo; z popravy Viléma je zoufalá
- **poutník Hynek**
  - Poutník, který se vrací na místo, kde byl popraven Vilém; depresivní,
    zoufalý, truchlivý, a to vše proto, že má také smutný osud v milostném
    vztahu; hodně zamyšlený i citlivý a srovnává se s Vilémem; dalo by se říct,
    že je do něj vlastně převtělený sám autor
- **otec Viléma**
  - Nečestný, svůdce Jarmily
- **Zvířata**
  - Promlouvají v 1. intermezzu při přípravě Vilémova pohřbu
- **Ostatní postavy**
  - sbor duchů, sbor loupežníků, žáby, krtek, rosa, noc, bouře a další

### Časoprostor

- Krajina u Doks pod Bezdězem, 1. pol. 19. století

### Vypravěcí způsoby

- er-forma

### Jazyk

- apostrofa
- archaismy
- básnický jazyk
- dramatická kompozice - vzestup, vrchol, sestup
- epiteta
- hovorové prostředky
- inverze
- jambický verš
- kontrast - jarní příroda x poprava, život x smrt
- lexikální prostředky - epiteton, metafora, metonymie, oxymóron, personifikace,
  přirovnání
- subjektivizace (vztah básníka a lyrického hrdiny)
- syntaktické prostředky - přímá řeč, monolog, dialog, zvukomalba
- útvary jazyka - spisovná čeština
- řečnická otázka
