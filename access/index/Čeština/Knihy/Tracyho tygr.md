### Autor

- [[William Saroyan]]

### Literární

- **druh:**
  - epika
- **forma:**
- **žánr:**
  - novela (symbolická pohádka pro dospělé)
- **směr:**
  - americká próza 2. poloviny 20. století

### Obsah

- Thomas Tracy toužil už od útlého mládí po tygrovi. Jednou ho viděl v ZOO, ale
  to ho vůbec nezaujalo. Zato když v patnácti letech uviděl v téže zoologické
  zahradě černého pantera, okamžitě v něm poznal svého tygra. Od té doby chodil
  tygr pořád s ním (panter samozřejmě zůstával ve své kleci v ZOO).
- S ním nastoupil Tracy i do kávových skladů. Zde pracoval čtrnáct dní a poté se
  šel zeptat nadřízeného, jestli by se z něho nemohl stát ochutnávač kávy, což
  byla nejžádanější pozice ve firmě. Při pohovoru ho doprovázel tygr a ten mu
  dodával odvahu. U konce rozhovoru ale tygr usnul a Tracy byl nakonec rád, že
  může alespoň nosit pytle s kávou.
- Před firmou jednou potkal Lauru Luthyovou a okamžitě se do ní zamiloval. Kolem
  firmy chodila každý den a začal se rozvíjet vztah, který skončil pozváním k
  Lauře domů. Tam ale Tracy podlehl svůdnosti Lauřiny matky a tím vztah s Laurou
  skončil. Tracy myslel, že takových dívek musí být spousta, ale brzy zjistil,
  že Laura byla jenom jedna.
- Zklamaný odjíždí do rodného San Francisca, ze kterého se ale po šesti letech
  opět vrací do New Yorku. S tygrem prochází místa, která před šesti lety
  opustil. Kávové sklady nahradily sklady nábytku. Při procházce městem zjistil,
  že od něho a jeho tygra lidé zděšeně utíkají. Tygr je brzy postřelen policií a
  Tracy se dovídá, že se jedná o černého pantera, který utekl z cirkusu. Tygrovi
  se podařilo utéct, ale Tracy byl zavřen do blázince.
- Zde se setkává s některými z ochutnávačů kávy z kávových skladů a také s
  Laurou. V blázinci jsou všichni smutní a Tracy se je snaží povzbudit. Policie
  mezitím marně pátrá po uprchlém panterovi. Tracy vymyslí plán, díky kterému by
  mohl najít tygra a možná také obnovit přátelství s Laurou.
- Jeho plán byl následovný: sklady nábytků se opět změní na sklady kávy, Tracy
  ze bude přenášet pytle s kávou a staří ochutnávači kávy budou ochutnávat kávu,
  tak, jako to bylo před šesti lety. A stejně jako před šesti lety, přejde i
  tento den kolem firmy Laura v nádherných šatech a brzy se objeví i tygr. Toho
  potom odvedou do speciálně upravené místnosti, kde pro něj bude připravená
  klec.
- Policie na tento zvláštní plán přistoupí. Kupodivu vše vychází přesně podle
  plánu. Dva policisté pozorují, jak Tracy s Laurou odvádí pantera do místnosti
  a po chvíli vychází sami ven a odchází pryč. Když se k místnosti dostanou,
  zjistí, že je prázdná a strážníci, kteří místnost monitorovali, potvrdí, že
  zde byl pouze Tracy s Laurou.

### Členění

- 15 kapitol

### Kompozice

- chronologická

### Téma

- Autor má rád tygry a jeho hrdinové jsou většinou mladíci, co něco hledají.
- Námětem je tedy hledání čehokoliv.

### Motivy

- život
- láska
- optimismus
- smutek

### Postavy

- **Thomas Tracy**
  - hlavní postava celého příběhu; již odmala pociťuje potřebu mít něco, co bude
    jeho součástí; nakonec se jím stává tygr, který tygrem vlastně vůbec není
    (je to černý panter); Thomas se zamilovává do Laury a později i přes rozchod
    s ní mu neustále leží v srdci; nakonec ji pohádkově znovu nalézá
- **Tracyho tygr**
  - černý panter, který zosobňuje vše, co člověk (Thomas) potřebuje; tvoří část
    Thomase; jedná se o jeho touhy a lásku; pomáhá mu proplouvat životem
- **Laura Luthyová**
  - Thomasova velká láska, se kterou nakonec najde své štěstí; Thomas jí podvede
    s její matkou; Laura se později zblázní a Thomas ji najde v blázinci i s
    její tygřicí
- **Doktor Pingitzer**
  - milý, usměvavý, asi sedmdesátiletý, známý a uznávaný psychiart, kouří dýmku
    a vyšetřoval Tracyho

### Časoprostor

- San Francisco, New York, ústav Bellevue (20.stol.)

### Vypravěcí způsoby

- er-forma

### Jazyk

- Pohádkovost
- Zvláštní kouzlo, pozitivní pohled na svět,
- Přímá řeč
- Krátké srozumitelné věty, které jsou nutné pro vyjádření složité skutečnosti
- Opakování stejných slov, vět
- Nekomplikovaná výstavba příběhu, jednoduchost, stručnost
- Nevšední téma, jednoduchá skladba vět a slovní zásoba,
- Základní vypravěčské postupy
- Použití germanismu, špatné skloňování angličtiny
- Není jasné, kde je hranice reality fantazie
- Satirické prvky, výsměch novinovým článkům, psychiatrickým postupům

### Syntax

-
