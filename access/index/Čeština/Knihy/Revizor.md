> [!important]- Na potítku
>
> <iframe allowfullscreen frameborder="0" width="640" height="360" src="https://www.youtube.com/embed/tS7t2-NjXEQ"></iframe>

### Autor

- [[Nikolaj Vasiljevič Gogol]]

### Literární

- **druh:**
  - drama
- **forma:**
  - drama
- **žánr:**
  - satirická komedie
- **směr:**
  - ruský realismus, 19. století

### Obsah

- Příběh Revizora se odehrává v nejmenovaném ruském městečku, na hony vzdáleném
  od zbytku civilizace, a je zasazen do počátku třicátých let 19. století. V
  této době těžkých vládních represí přichází hejtmanovi Antonu Antonovičovi
  dopis varující před příchodem revizora, jenž má cestovat v utajení -
  "inkognito". Poté, co tuto zprávu rozšíří mezi místní honoraci, přibíhají
  uřícení statkáři Bobčinskij a Dobčinskij s informací o petrohradském
  úředníkovi, který se před dvěma týdny ubytoval ve zdejším hostinci a chová se
  velmi podezřele. Všichni ho okamžitě začnou považovat za revizora a hejtman se
  za ním rychle vypraví. Chlestakov mezitím žije na dluh a snaží se vyprosit si
  u hostinského alespoň trochu jídla. Po příchodu hejtmana je trochu zmatený,
  neboť očekával, že ho jdou zatknout. Když však spatří, že se mu všichni klaní
  a jednají s ním jako v rukavičkách, rozhodne se toho využít a začne si
  stěžovat na mizerné pohostinství, kterého se mu dostalo. Anton Antonovič se
  bojí jeho hněvu a hledí, jak si ho udobřit. Navrhne mu tedy, aby se
  přestěhoval do jeho domu. Chlestakov přijímá a s radostí zneužívá
  vyskytnuvšího se omylu, aby ze svých hostitelů dostal co nejvíc peněz, které
  mu všichni ochotně "půjčují". Obzvlášť poté, co jim namluví, že patří k
  nejmocnějším mužům hlavního města. Osipovi je však jasné, že pohádce o
  úředníkovi z Petrohradu, jenž je jedna ruka s ministrem, nikdo moc dlouho
  věřit nebude, a domlouvá proto svému pánovi, aby co nejrychleji odjeli.
  Chlestakov po chvíli přemlouvání souhlasí, ale napíše ještě svému dobrému
  příteli Trapičkinovi o této veselé příhodě dopis. Osip z dopisem odejde a náš
  domnělý revizor se mezitím začne dvořit hejtmanově dceři Marje Antonovně a
  požádá otce o její ruku. Ten zpočátku nevěří svým uším, ale pak nadšeně
  souhlasí a spolu s manželkou se oddává představám o budoucím životě v
  Petrohradu. Vtom jim Chlestakov sdělí, že musí naléhavě odjet za svým strýcem.
  Půjčí si nejrychlejší koně a urychleně se vytratí. V hejtmanově domě se
  mezitím začíná scházet městská smetánka a blahopřeje Antonu Antonovičovi k
  jeho štěstí. Gratulace však přeruší svým příchodem poštmistr, který rozlepil
  Chlestakovův dopis. Z něj se všichni dovídají, jak se nechali obelstít. V
  listu je navíc obsažen karikující popis jednotlivých obyvatel městečka, což
  samozřejmě všechny přítomné rozzlobí, nejvíce pak hejtmana. Do toho přichází
  četník se vzkazem od pravého revizora, jenž se před chvílí ubytoval v
  hostinci. Tato zpráva způsobí všeobecné zděšení a šok.

### Členění

- 5 aktů

### Kompozice

- chronologická

### Téma

- peníze
- záměna postav (a komické situace s tím spojené)

### Motivy

- nešvary společnosti
- úplatkářství
- podvody
- korupce
- moc
- nedorozumění
- strach

### Postavy

- **Hejtman**
  - Velitel policejního sboru, postarší muž s krátkými, prošedivělými vlasy;
    bere úplatky, k obyvatelům města se chová tvrdě, neváhá kdekoho obrat o
    poslední kopějku; přesto si zachovává jistou vážnost, vždy nosí uniformu, v
    neděli dokonce chodí do kostela; zpráva o příchodu revizora ho velmi
    znepokojuje, je pod velkým tlakem a ztrácí svou obezřetnost; proto v
    Chlestakovovi nepozná obyčejného městského hejska a snaží se mu co nejvíce
    zalichotit a podplatit ho; když po jeho odjezdu zjišťuje, jak snadno se on,
    zkušený a protřelý policista, nechal napálit, propadá zoufalství a zuřivosti
- **Chlestakov**
  - Třiadvacetiletý hubený mladík, pracoval v Petrohradu jako úředník a teď se
    vrací na venkov za svým otcem; není příliš bystrý, nemá vyšší cíle, rád
    pije, kouří a hraje karty; díky svému módnímu obleku je pokládán za
    revizora, což mu zprvu nedochází, ale přesto využije omylu ve svůj prospěch;
    nikdo ho neomezuje, má tedy možnost naplno se rozvinout a ukázat svůj
    charakter, který je však velmi nevýrazný; zcela se proto vžívá do své role
    státního úředníka a téměř nevnímá, že vypráví bohapusté lži - to jim logicky
    dodává na hodnověrnosti; nebýt svého sluhy Osipa, čekal by ve městě až do
    odhalení
- **Anna Andrejevna**
  - Hejtmanova manželka, žena z lepší rodiny, nepříliš stará; až přespříliš
    zvědavá a také vychloubačná; ke svému muži se chová přezíravě, dceru Marju
    Antonovnu neustále komanduje a zesměšňuje; Chlestakovem je zcela uchvácena,
    imponuje jí především jeho společenské postavení, dokonce by neváhala kvůli
    němu opustit manžela
- **Soudce**
  - Vzdělanější než většina ostatních postav; namyšlený, snaží se za každé
    situace vypadat důležitě; nebere peněžité úplatky, ale je vášnivým lovcem a
    s klidným srdcem by změnil rozsudek ve prospěch toho, kdo mu daruje štěně
    loveckého psa
- **Správce chudobince**
  - I přes svou tloušťku a neohrabanost reaguje velmi pohotově, umí skvěle
    podlézat a využít každou situaci ve svůj prospěch; ve vlastním zájmu neváhá
    udat ani své přátele a příbuzné
- **Školní inspektor**
  - Neprůbojný, ustrašený člověk, ze všech zúčastněných má z revizora největší
    strach, při rozhovoru s ním se neustále třese a koktá
- **Bobčinskij a Dobčinskij**
  - Oba jsou podsadití, menšího vzrůstu, mají stejné křestní jméno (Petr
    Ivanovič) a stejnou zálibu - roznášet klepy; z tohoto důvodu je znají i
    bohatší vrstvy, které od nich získávají informace o dění ve městě; mluví
    překotně a kvapně, Bobčinskij dokonce trochu koktá
- **Poštmistr**
  - Naivní, prostoduchý, nesoustředěný, stručně řečeno jednoduchý; v každé scéně
    vystupuje opilý, jeho jedinou zábavou je tajně rozlepovat a číst cizí dopisy
- **Osip**
  - Chlestakovův sluha, mazaný člověk, sice již starší, avšak o to protřelejší a
    drzejší; ke svému pánovi nechová příliš velkou úctu a často odmlouvá, ale
    přesto ho má rád a snaží se, aby nepřišel k nějaké újmě

### Časoprostor

- Nejmenované ruské město, 19. století

### Vypravěcí způsoby

- er-forma

### Jazyk

- archaismy
- citoslovce
- hovorová i spisovná čeština
- metafora
- přímá řeč
- rčení
