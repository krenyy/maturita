### Autor

- [[Paulo Coelho]]

### Literární

- **druh:**
  - epika
- ## **forma:**
- **žánr:**
  - psychologický román
- **směr:**
  - magický realismus, 2. polovina 20. století

### Obsah

-

### Členění

-

### Kompozice

- chronologická
- misty retrospektivní

### Téma

- pastýř putující za svým snem

### Motivy

- svět
- láska
- mysl
- úcta k přírodě a bohu
- osud

### Postavy

- ## **Postava**

### Časoprostor

-

### Vypravěcí způsoby

-

### Jazyk

-

### Syntax

-
