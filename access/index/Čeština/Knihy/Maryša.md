> [!important]- Na potítku
>
> <iframe allowfullscreen frameborder="0" width="640" height="360" src="https://www.youtube.com/embed/qbLy63T7kS8"></iframe>

### Autor

- [[Alois Mrštík]]
- [[Vilém Mrštík]]

### Literární

- **druh:**
  - drama
- **forma:**
  - divadelní hra
- **žánr:**
  - tragédie
- **směr:**
  - realismus, 19. století

### Obsah

- Maryša je dcera bohatého Lízala. Miluje Francka, který je ale velmi chudý, a
  proto Maryšin otec Lízal vztahu mladé dvojice nepřeje. Naopak chce dceru
  provdat za vdovce mlynáře Vávru, který vlastní celý mlýn a několik polí, ale
  je o dost starší než Maryša. Lízalovi se proto velmi hodí, že Francek odchází
  na vojnu. Francek se přišel za Maryšou rozloučit, ale Lízal ho od domu zahnal.
  A tak se Francek vrátil ještě jednou a s Maryšou se rozloučil a slíbil ji
  věrnost.
- Mezitím vyjednává Lízal sňatek Maryši s mlynářem Vávrou, který chtěl po něm
  určitou sumu peněz pro Maryšu, aby mohl zaplatit své dluhy. Lízalovi se to
  zdálo moc, ale nakonec se domluvili. Když se Maryša dověděla, koho si má vzít,
  prosila rodiče, aby ji Vávrovi nedávali. Matka jí domlouvala, že se s mlynářem
  bude mít dobře, že mlýn mu dobře vynáší a Franckovi, tonu ji nedá. Maryša se
  však obává, že ji Vávra bude bít stejně jako předchozí ženu a navíc je na ni
  příliš starý a vůbec se ji nelíbí.
- Když přišel svatební den, přijel si pro ni Vávra s kočárem. Maryša jej
  prosila, aby si ji nebral, ať jí nekatí život. Ten jí sliboval, že se bude u
  něho mít dobře, že jí udělá vše, co jí na očích uvidí. A tak nezbývalo Maryši
  než si mlynáře vzít. Vávra se k ní nechová vůbec pěkně, bije ji, protože ví,
  že ho nemiluje. Maryša na své rodiče úplně zanevřela, protože jim nedokáže
  odpustit, že ji prodali tak špatnému člověku. Jednou, když ji Lízal uvidí, jak
  jde manželovi pro pivo, jen se na sebe podívají, ani slovo si neřeknou.
  Sedlákovi přinese sluha lístek od soudu, kde stojí, že ho Vávra žaluje pro
  peníze.
- Za tři roky se vrátil Francek z vojny. Na nic nedbal a vydal se za Maryšou, i
  když věděl, že je vdaná. Vždyť ji stále miloval. Ani ji nemůže poznat. Maryša
  je bledá, hubená a smutná. Dá se s ní do řeči a jde ji doprovodit. Tím chce
  ukázat celé vesnici, ale hlavně Vávrovi, že se Maryši nevzdá a že na ni
  nezapomněl. Lízal si uvědomil, jak jeho dcera žije a proto se rozhodl, že ji
  vezme domů. Maryša však nechtěla, protože věří ve svátost manželskou a
  nechtěla udělat svým rodičům ostudu. Přemlouval Maryšu, aby s ním odjela do
  Brna. Tam je pro oba práce zajištěná. Maryša, i když ho milovala tak se bála.
  Francek se s ní chce sejít v noci u splavu, ale Maryša nechce, také se bojí,
  že by ho mohl Vávra zastřelit, ale to Franckovi vůbec nevadí.
- Když se Vávra vrátí domů z hospody, tuší, že Maryša někoho měla doma. Maryša
  mu nic nechce prozradit a zatlouká. Je si docel jistý, že to byl Francek.
  Vezme pušku a jde ke splavu. Vávra po Franckovi střílí, ale nedokáže se
  trefit. Maryša byla rozhodnuta jinak. Ráno, když Vávra odcházel do své práce,
  uvařila mu žena kávu, do které nasypala jed. Vávra se napil a začal vlídně
  hovořit k ženě. Sliboval, že už se nebudou hádat a budou spolu žít spořádaným
  životem. Vávra dopil zbytek kávy a klesl k zemi mrtev. Když přišli sousedé a
  viděli mrtvého mlynáře, Maryša se ke všemu přiznala.

### Členění

- pět jednání s proměnou (třetí jednání je o dvě léta později než jednání první)

### Kompozice

- chronologická

### Téma

- Nucený, nešťastný sňatek

### Motivy

- láska
- násilí
- otrava
- peníze
- vražda

### Postavy

- **Maryša**
  - mladá dívka, která je zamilovaná do Francka
  - poslušná dcera Lízala
  - na příkaz svých rodičů a na tlak z jejich strany strany si bere Vávru
  - musí je uposlechnout, vykoná tak čin proti své vůli, jen aby uposlechla
    všechny náboženské a společenské zásady
  - sňatek je pro ni jen životním neštěstím.
  - je velmi tvrdohlavá (ke konci děje nechce utéct s Franckem do Brna raději
    trpí s Vávrou-nemiluje ho a vávra ji mlátí)
- **Lízal**:
  - postava starého sedláka který viděl dřív peníze a potom štěstí svého dítěte
  - později svých činů litoval a chtěl vše napravit
  - Maryša však už o nápravu nejevila zájem a otci řekla že na to měl myslet již
    dřív, než ji provdal, ale nyní je již pozdě.
- **Ostatní postavy**:
  - Francek, Vávra, Lízalová, Horačka (matka Francka), Hospodský, Strouhalka
    (teta Maryši), Babička(Maryši)

### Časoprostor

- Moravská vesnice
- Konec 19. století

### Vypravěcí způsoby

- er-forma

### Jazyk

- nespisovný
- nářečí (kombinace Hanáckého a Slováckého a brněnského dialektu)
- hovorové výrazy
- archaismy
- obrazná pojmenování:
  - metafora
  - personifikace
  - přirovnání
  - epizeuxis
  - hyperbola,
- elipsa (vynechání části věty - která je známa)
- syntax věty jednoduché i souvětí, holé věty
- všechny druhy vět (oznamovací,rozkazovací,přací,tázaci.zvolací)
- přímá řeč, monolog, dialog
