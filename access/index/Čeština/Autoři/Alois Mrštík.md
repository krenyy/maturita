### O autorovi

- Prozaik, dramatik
- Vystudoval učitelský ústav
- Učil v několika jihomoravských obcích
- Později byl jmenován správcem školy v Divákách, kde žil až do své smrti
- Přispíval do Lumíru, Světozoru, Národních listů aj.

### Díla

- [[Bavlnkova žena]]
- [[Dobré duše]]
- [[Maryša]]
- [[Rok na vsi]]

### Současníci

- [[Alexandr Sergejevič Puškin]]
- [[Alois Jirásek]]
- [[Charles Dickens]]
- [[Karel Václav Rais]]
- [[Ladislav Stroupežnický]]
- [[Nikolaj Vasiljevič Gogol]]
- [[Vilém Mrštík]]
