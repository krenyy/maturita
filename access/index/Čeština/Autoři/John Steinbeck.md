### O autorovi

- Celým jménem John Ernest Steinbeck
- Narodil se roku 1902 v kalifornském městě Salinas.
- Je považován za jednoho z největších nejen amerických, ale i světových
  spisovatelů.
- Pocházel z poměrně chudých poměrů
- Nastoupil na Stanfordskou univerzitu, ale studium historie a anglické
  literatury nedokončil a vrátil se k práci na velkostatku.
- Jeho touha stát se spisovatelem ho však táhla jinam, a tak odešel jako
  reportér do New Yorku, ale kvůli neúspěchům novinařiny nechal.
- Střídal různá zaměstnání a své zkušenosti pak využíval do svých románů.
- Jeho romány je třeba chápat jako reakci na krizi, nehledá řešení, omezuje se
  na popis bídy.
- Patří mezi autory
  [ztracené generace](Ztracená%20generace%20(Lost%20generation)).

### Díla

- [[O myších a lidech]]
- [[Neznámemu bohu]]
- [[Pláň Tortilla]]
- [[Hrozny hněvu]]
- [[Perla]]
- [[Na plechárně]]
- [[Toulavý autobus]]
- [[Ryzáček]]
- [[Na východ od ráje]]

### Současníci

- [[Ernest Hemingway]]
- [[Erich Maria Remarque]]
- [[Francis Scott Fitzgerald]]
- [[William Faulkner]]
- [[Mark Twain]]
- [[Ivan Olbracht]]
- [[Marie Majerová]]
- [[Karel Čapek]]
