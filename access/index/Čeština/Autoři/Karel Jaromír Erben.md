### O autorovi

- čech
- narozen v Miletíně, Rakouské císařství
- básník, spisovatel, historik
- psal pohádky
- představitel literárního romantismu
- patřil mezi významné osobnosti českého národního obrození
- měl poruchy řeči
- zemřel na tuberkulózu

### Díla

- [[Dlouhý, Široký a Bystrozraký]]
- [[Kytice]]
- [[O Zlatovlásce]]
- [[Tři zlaté vlasy děda Vševěda]]
- [[České pohádky]]

### Současníci

- [[Alexandr Sergejevič Puškin]]
- [[Alexandre Dumas]]
- [[Božena Němcová]]
- [[Edgar Allan Poe]]
- [[František Palacký]]
- [[Jacob Grimm]]
- [[Josef Kajetán Tyl]]
- [[Karel Havlíček Borovský]]
- [[Karel Hynek Mácha]]
- [[Viktor Hugo]]
- [[Wilhelm Grimm]]
