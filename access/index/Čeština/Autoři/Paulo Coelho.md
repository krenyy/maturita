### O autorovi

- ñic

### Díla

- [[Vyzvědačka]]
- [[Ďábel a slečna Chantal]]
- [[Čarodějka z Portobella]]
- [[Pátá hora]]

### Současníci

- [[John Ronald Reuel Tolkien]]
- [[John Irving]]
- [[Umberto Eco]]
- [[Alberto Moravia]]
- [[Vladimír Nabokov]]
- [[William Saroyan]]
- [[Jiří Kratochvíl]]
