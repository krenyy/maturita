### O autorovi

- :(

### Díla

- [[Jan Kryštof]]
- [[Okouzlená duše]]
- [[Dobrý člověk ještě žije]]
- [[Hra o lásce a smrti]]
- [[Beethovenův život]]
- [[Michelangelo]]

### Současníci

- [[Ernest Hemingway]]
- [[Erich Maria Remarque]]
- [[Arnold Zweig]]
- [[Henri Barbusse]]
- [[George Orwell]]
