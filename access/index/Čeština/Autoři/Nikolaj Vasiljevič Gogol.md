### O autorovi

- rus
- narozen v Soročincích, Ruské impérium, současná Ukrajina
- básník, dramatik, historik, literární kritik, prozaik, učitel
- pocházel z rodiny středních statkářů
- po absolvování gymnázia odešel koncem roku 1828 do Petrohradu, kde byl přijat
  do divadla
- pokoušel se vydávat básně a uspořádat výstavu svých obrazů, setkal se z
  vysokou kritikou
- nakonec působil jako úředník
- před svou smrtí prožíval hlubokou depresi
- na jeho tvorbu navazuje prakticky celá ruská literatura 2. poloviny 19.
  století
- za nejlepší z jeho děl jsou považovány povídky, kde plně využil své
  vypravěčské umění a schopnost spojovat reálné s fantastickým

### Díla

- [[Arabesky]]
- [[Mirgorod]]
- [[Mrtvé duše]]
- [[Několik slov o Puškinovi]]
- [[Petrohradské povídky]]
- [[Revizor]]
- [[Večery na samotě u Dikaňky]]
- [[Ženitba]]

### Současníci

- [[Alexandr Sergejevič Puškin]]
- [[Alois Mrštík]]
- [[Anton Pavlovič Čechov]]
- [[Charles Dickens]]
- [[Fjodor Michajlovič Dostojevskij]]
- [[Gustav Flaubert]]
- [[Honoré de Balzac]]
- [[Lev Nikolajevč Tolstoj]]
- [[Mark Twain]]
- [[Vilém Mrštík]]
