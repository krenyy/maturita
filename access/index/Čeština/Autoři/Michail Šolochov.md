### O autorovi

- Pocházel z Kružilina v Rostovské oblasti a na Donu strávil většinu života.
- Rodnému kraji zasaženému občanskou válkou i dalšími společenskými proměnami
  věnoval Šolochov podstatnou část tvorby.
- Studoval na gymnáziu, kde napsal své první verše.
- V roce 1922 odešel do Moskvy.
- V roce 1932 vstoupil do komunistické strany Sovětského svazu, od roku 1961 byl
  volen za člena ústředního výboru KSSS.
- Již v roce 1934 byl členem vedení Svazu sovětských spisovatelů a roku 1939 se
  stal členem Akademie věd.
- Byl také poslancem Nejvyššího sovětu SSSR.
- Druhé světové války se účastnil jako válečný dopisovatel deníku Pravda.
- Za čtyřdílný Tichý Don byl dekorován vysokým státním vyznamenáním.
- V roce 1960 mu byla udělena Leninova cena.
- V roce 1965 obdržel Šolochov za své celoživotní dílo Nobelovu cenu.

### Díla

- [[Osud člověka]]
- [[Donské povídky]]
- [[Azurová step]]
- [[Tichý Don]]
- [[Rozrušená země]]
- [[Brániti svou zem]]

### Současníci

- [[Boris Polevoj]]
- [[Alexandr Fadějev]]
- [[William Styren]]
- [[P. Ryan]]
- [[J. Heller]]
- [[Maxim Gorkij]]
- [[Michail Bulgakov]]
- [[Boris Pasternak]]
- [[Ivan Olbracht]]
- [[John Steinbeck]]
- [[Ernest Hemingway]]
- [[Marie Majerová]]
- [[Romain Rolland]]
- [[Karel Čapek]]
