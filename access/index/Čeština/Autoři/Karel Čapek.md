> [!important]- Na potítku
>
> <iframe allowfullscreen frameborder="0" width="640" height="360" src="https://www.youtube.com/embed/GgonEtbC6T8"></iframe>
> <iframe allowfullscreen frameborder="0" width="640" height="360" src="https://www.youtube.com/embed/GLHcRXZ4CUU"></iframe>

### O autorovi

- Narozen 9. ledna 1890 v Malých Svatoňovicích
- Patří k nejvýznamnějším českým dramatikům, prozaikům, novinářům a
  překladatelům 20. století.
- Studoval filozofii na UK v Praze i v Paříži a Berlíně
- Kvůli své nemoci nemusel narukovat a bojovat v 1. sv. válce, přesto jej
  ovlivnila
- Během války začal pracovat jako redaktor, například v Národních listech a
  Lidových novinách
- Po válce působil také jako režisér a dramaturg Vinohradského divadla, následně
  byl předsedou československého svazu spisovatelů.
- Ve 30. letech si uvědomoval nebezpečí fašismu a upozorňoval na ně ve svém díle
- Podepsání Mnichovské dohody pak bral jako národní i osobní tragédii
- Otevřeně hájil národní jednotu a svobodu
- Zemřel na následky nachlazení ještě před začátkem války 25. prosince 1938.
- Zajímal se o fotografování, etnickou hudbu a cizí kultury, podnikl několik
  cest po Evropě
- Jeho manželkou byla herečka a spisovatelka Olga Scheinpflugová

### Díla

- Cestopisy:
  - [[Cesta na sever]]
- Dramata:
  - [[Ze života hmyzu]]
  - [[Bílá nemoc]]
  - [[R.U.R.]]
- Novely:
  - [[Život a dílo skladatele Foltýna]]
- Pohádky:
  - [[Dášenka čili život štěněte]]
- Romány:
  - [[Válka s mloky]]
  - [[Továrna na absolutno]]
- Sbírky povídek:
  - [[Povídky z jedné kapsy]]
  - [[Povídky z druhé kapsy]]
  - [[Trapné povídky]]

### Současníci

- [[Eduard Bass]]
- [[Ferdinand Peroutka]]
- [[Jarmila Glazarová]]
- [[Jaromír John]]
- [[Josef Čapek]]
- [[Karel Poláček]]
- [[Lev Blatný]]
- [[Rudolf Medek]]
- [[Michail Šolochov]]
- [[Ernest Hemingway]]
- [[Erich Maria Remarque]]
