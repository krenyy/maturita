> [!info]+ Kontext autorovy tvorby
>
> - vlastním jménem Jean Baptiste Poquelin ^1
> - herec, režisér, divadelní ředitel, právnické vzdělání
> - jeho divadlo získalo přídomek královské
> - díky panovníkově přízni Moliére překonal opakované spory s vysokou šlechtou a církví
> - celý život se pokoušel vytvořit velkou tragédii, proslavil se však komediemi, ve kterých často překračuje klasicistické normy
> - vycházel z antických komedií (Plautových), lidových frašek, italské commedie dell'arte
> - jeho postavy nejsou neměnnými typy, jsou schopny vývoje, názorové proměny
> - i urozené postavy nechal mluvit prózou
> - ve svých hrách kritizuje a zesměšňuje pokrytectví, nepřirozenost, mužské pojetí lásky k ženě jako vztah pána a podřízené, ženskou afektovanost, lidskou hloupost, šarlatánství a hypochondrii spojenou s vypočítavostí
> - zemřel na jevišti při hře [[Zdravý nemocný]]

> [!info]+ Literární / obecně kulturní kontext
>
> - francouzský klasicismus 17. a 18. století
> - napodoboval formy antického umění
> - jednota času, děje a místa, racionalita
>
> > [!example]+ Díla
> >
> > - [[Don Juan]]
> > - [[Hraběnka z Nouzova]]
> > - [[Lakomec]]
> > - [[Misantrop]]
> > - [[Měšťák šlechticem]]
> > - [[Scapinova šibalství]]
> > - [[Tartuffe]]
> > - [[Zdravý nemocný]]
> > - [[Škola žen]]
>
> > [!example]+ Současníci
> >
> > - [[Carlo Goldoni]]
> > - [[Daniel Defoe]]
> > - [[Denis Diderot]]
> > - [[Jean Racine]]
> > - [[Jean de La Fontaine]]
> > - [[Jonathan Swift]]
> > - [[Pierre Corneille]]
> > - [[Voltaire]]
>
> > ![[Klasicismus]]
