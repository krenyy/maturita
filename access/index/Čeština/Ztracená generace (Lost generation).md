- Skupina amerických spisovatelů narozených kolem roku 1900, kteří zažili první
  světovou válku a tuto skutečnost zobrazovali ve svých dílech.
- Vyjadřují pocity vojáků po návratu z války.
- Pro tyto autory jsou typické životní postoje, kterými dávají najevo nesouhlas
  a nedůvěru ke společnosti.

- Autoři:
  - [[John Steinbeck]]
  - [[Ernest Hemingway]]
  - [[William Faulkner]]
  - [[Francis Scott Fitzgerald]]
  - [[John Doss Passos]]
  - [[Erich Maria Remarque]]
  - [[Thomas Stearns Eliot]]
  - [[Ezra Pound]]
  - [[Eugene O'Neill]]
