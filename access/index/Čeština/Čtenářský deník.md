Zvýrazněná díla jsou mnou vybraná, tudíž na ně bude brán větší zřetel.

### Světová a česká literatura do konce 18. století (nejméně 2 díla)

> [!important] [[Lakomec]] - [[Molière]]

> [!important] [[Romeo a Julie]] - [[William Shakespeare]]

### Světová a česká literatura 19. století (nejméně 3 díla)

> [!important] [[Kytice]] - [[Karel Jaromír Erben]]

> [!important] [[Revizor]] - [[Nikolaj Vasiljevič Gogol]]

> [!important] [[Máj]] - [[Karel Hynek Mácha]]

> [!important] [[Maryša]] - [[Alois Mrštík]], [[Vilém Mrštík]]

> [!important] [[V zámku a podzámčí]] - [[Božena Němcová]]

### Světová literatura 20. a 21. století (nejméně 4 díla)

> [!cite] [[Alchymista]] - [[Paulo Coelho]]

> [!important] [[Malý princ]] - [[Antoine de Saint-Exupéry]]

> [!important] [[Stařec a moře]] - [[Ernest Hemingway]]

> [!important] [[Farma zvířat]] - [[George Orwell]]

> [!important] [[Petr a Lucie]] - [[Romain Rolland]]

> [!important] [[Tracyho tygr]] - [[William Saroyan]]

> [!important] [[O myších a lidech]] - [[John Steinbeck]]

> [!important] [[Osud člověka]] - [[Michail Šolochov]]

### Česká literatura 20. a 21. století (nejméně 5 děl)

> [!important] [[Bílá nemoc]] - [[Karel Čapek]]

> [!important] [[R.U.R.]] - [[Karel Čapek]]

> [!important] [[Krysař]] - [[Viktor Dyk]]

> [!important] [[Ostře sledované vlaky]] - [[Bohumil Hrabal]]

> [!important] [[Saturnin]] - [[Zdeněk Jirotka]]

> [!cite] [[Bylo nás pět]] - [[Karel Poláček]]

> [!important] [[Báječná léta pod psa]] - [[Michal Viewegh]]
