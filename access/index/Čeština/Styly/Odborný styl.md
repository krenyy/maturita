> styl odborných publikací, časopisů, učebnic, aj.

##### Podle stupně odbornosti rozlišujeme

- styl populárně naučný (pro laiky a začátečníky v oboru)
- styl prakticky odborný (pro poučenější příjemce)
- styl vědecký (pro odborníky a specialisty v konkrétním oboru)

### Funkce

- odborně sdělná
- vzdělávací

### Forma

- zpravidla psaná, ale i mluvená

### Cíl

- poučit
- předat přesné informace

### Požadavky

- věcná správnost
- přísná objektivita
- přehlednost

### Slohový postup

- informační
- popisný
- výkladový
- úvahový

### Slohové útvary

- výklad
- odborná úvaha
- encyklopedické heslo
- referát
- diplomová práce
- učebnice
- odborný popis

### Jazyk

- spisovný jazyk
- odborné výrazy (termíny):
  - jednoslovné
  - víceslovné
  - domácí
  - přejatá
- shrnující výrazy (slovní druhy, Newtonovy zákony)
- diferencující výrazy (vražda (úmyslná) / zabití (neúmyslné))
- zkratky
- značky
- závorky

### Syntax

- věty jednoduché
- souvětí
- pořádek ve větě
- členění na odstavce, vyšší úseky a kapitoly
- podtrhávání
- druhy písma
- číslování
- tabulky
- grafy
