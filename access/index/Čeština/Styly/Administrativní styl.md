> styl úředního jednání

### Funkce

- řídící (předpisy, zákony)
- odborně sdělná (zpráva)
- správní (žádost)

### Forma

- převážně psaná, ale i mluvená (projev)

### Cíl

- neny

### Požadavky

- výstižnost
- srozumitelnost
- stručnost

### Slohový postup

- informační
- popisný
- výkladový

### Slohové útvary

- stížnost
- životopis
- zákony
- žádost
- protokol
- předpis
- předtištěný formulář

### Jazyk

- spisovný
- neutrální
- odborné názvy (termíny):
  - jednoslovné
  - víceslovné
  - domácí
  - přejaté
- zkratky
- zkratková slova
- značky
- pasivní slovesné tvary (bylo rozhodnuto, bylo ověřeno)
- autorský plurál a vykání (obracíme se na Vás)
- ustálené formulace (vstoupit v platnost, na základě jednání)
- heslovité vyjadřování

### Syntax

- věty jednoduché (oznamovací)
- souvětí
