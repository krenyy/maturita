1. V pravoúhlém $\triangle ABC$ je dáno: $\gamma=90\textdegree, b=10\ cm, v_c=8\ cm$.

   - Určete obvod a obsah tohoto trojúhelníku.

   > [!done]- Řešení
   > ![[./plots/0501.png]]
   >
   > $$
   > \begin{aligned}
   >   \gamma &= 90\degree \\
   >   b &= 10\ cm \\
   >   v_c &= 8\ cm \\
   >   \\
   >   \frac{b}{\sin\left(90\degree\right)} &= \frac{v_c}{\sin\left(\alpha\right)} \\
   >   \frac{10}{\sin\left(90\degree\right)} &= \frac{8}{\sin\left(\alpha\right)} \\
   >   10 &= \frac{8}{\sin\left(\alpha\right)} \\
   >   10\sin\left(\alpha\right) &= 8 \\
   >   \sin\left(\alpha\right) &= 0.8 \\
   >   \alpha &= \arcsin\left(0.8\right) \\
   >   \\
   >   \beta &= 180\degree-\gamma-\alpha \\
   >   \beta &= 90\degree-\arcsin\left(0.8\right) \\
   >   \\
   >   \frac{c}{\sin\left(\gamma\right)} &= \frac{b}{\sin\left(\beta\right)} \\
   >   c &= \frac{10}{\sin\left(90\degree-\arcsin\left(0.8\right)\right)} \\
   >   c &= \frac{50}{3} \\
   >   \\
   >   \frac{a}{\sin\left(\alpha\right)} &= \frac{c}{\sin\left(\gamma\right)} \\
   >   \frac{a}{\sin\left(\arcsin\left(0.8\right)\right)} &= \frac{50}{3} \\
   >   a &= \frac{40}{3} \\
   >   \\
   >   o &= a+b+c \\
   >   o &= \frac{40}{3}+10+\frac{50}{3} \\
   >   o &= 40\ cm \\
   >   \\
   >   A &= \frac{cv_c}{2} \\
   >   A &= \frac{\frac{50}{3}\times 8}{2} \\
   >   A &= \frac{200}{3}\ cm^{2}
   > \end{aligned}
   > $$

2. Obecný lichoběžník $ABCD$ má délky stran: $a=30cm, b=13cm, c=16cm, d=15cm$.

   - Vypočítejte jeho obsah.

   > [!done]- Řešení
   > ![[./plots/0502.png]]
   > ![[./plots/0502-1.png]]
   >
   > - $$
   >   \begin{aligned}
   >     p_\triangle &= 42\ cm \\
   >     s_\triangle &= \frac{p_\triangle}{2} \\
   >     s_\triangle &= 21\ cm \\
   >     \\
   >     A_\triangle &= \sqrt{s_\triangle\left(s_\triangle-a_\triangle\right)\left(s_\triangle-b_\triangle\right)\left(s_\triangle-c_\triangle\right)} \\
   >     A_\triangle &= 84\ cm^{2} \\
   >     \\
   >     A_\triangle &= \frac{hc}{2} \\
   >     h &= 12\ cm \\
   >     \\
   >     A &= \frac{a+c}{2}h \\
   >     A &= 276\ cm^{2}
   >   \end{aligned}
   >   $$

3. Kosočtverec má obsah $S=150\ cm^{2}$, délky jeho úhlopříček jsou v poměru $3:4$.

   - Určete délku strany kosočtverce a jeho výšku.

   > [!done]- Řešení
   > ![[./plots/0503.png]]
   >
   > - $$
   >   \begin{aligned}
   >     u_1 &= \frac{4}{3}x \\
   >     u_2 &= x \\
   >     A &= 150\ cm^{2} \\
   >     \\
   >     A &= \frac{1}{2}u_1u_2 \\
   >     x &= 15\ cm \\
   >     \\
   >     u_1 &= 20\ cm \\
   >     u_2 &= 15\ cm \\
   >     \\
   >     a^{2} &= \left(\frac{u_1}{2}\right)^{2}+\left(\frac{u_2}{2}\right)^{2} \\
   >     4a^{2} &= u_1^{2}+u_2^{2} \\
   >     4a^{2} &= 625 \\
   >     a^{2} &= 156.25 \\
   >     a &= 12.5\ cm \\
   >     \\
   >     A &= av \\
   >     150 &= 12.5v \\
   >     v &= 12
   >   \end{aligned}
   >   $$

4. Je zadán rovnostranný $\triangle$ se stranou délky $12\ cm$.

   - Vypočítejte plochu mezikruží tvořeného kružnicí opsanou a vepsanou tomuto $\triangle$.

   > [!done]- Řešení
   > ![[./plots/0504.png]]
   >
   > - $$
   >   \begin{aligned}
   >     a &= 12 \\
   >     p &= 3a \\
   >     \\
   >     a^{2} &= \left(\frac{a}{2}\right)^{2}+v^{2} \\
   >     v &= 6\sqrt{3} \\
   >     \\
   >     A_\triangle &= \frac{av}{2} \\
   >     A_\triangle &= 36\sqrt{3} \\
   >     \\
   >     A_\triangle &= \frac{pr}{2} \\
   >     r &= 2\sqrt{3} \\
   >     \\
   >     r_2 &= v-r \\
   >     r_2 &= 4\sqrt{3} \\
   >     \\
   >     A_1 &= \pi r^{2} \\
   >     A_2 &= \pi r_2^{2} \\
   >     A &= A_2 - A_1 \\
   >     \\
   >     A_1 &= 12\pi \\
   >     A_2 &= 48\pi \\
   >     A &= 36\pi
   >   \end{aligned}
   >   $$

5. Jakou část plochy kruhu zaujímá pravidelný šestiúhelník, který je tomuto kruhu vepsaný?

   > [!done]- Řešení
   >
   > - $$
   >   \begin{aligned}
   >     r &= a \\
   >     \\
   >     a^{2} &= \left(\frac{a}{2}\right)^{2} + h^{2} \\
   >     h &= \frac{a\sqrt{3}}{2} \\
   >     \\
   >     6A_\triangle &= xA_\circ \\
   >     6\frac{ah}{2} &= x\pi r^{2} \\
   >     3ah &= x\pi r^{2} \\
   >     3ah &= x\pi a^{2} \\
   >     3h &= x\pi a \\
   >     3\frac{a\sqrt{3}}{2} &= x\pi a \\
   >     3a\sqrt{3} &= 2ax\pi \\
   >     3\sqrt{3} &= 2x\pi \\
   >     x &= \frac{3\sqrt{3}}{2\pi} \\
   >     x &\approx 0.8269933431
   >   \end{aligned}
   >   $$

6. Je dán pravidelný devítiúhelník ABCDEFGHI.

   - Vypočítejte velikosti vnitřních úhlů čtyřúhelníku ACDG.

   > [!todo]- Řešení
   > ![[./plots/0506.png]]

   - Určete odchylku přímek AF a BH.

   > [!todo]- Řešení
   > ![[./plots/0506-1.png]]
