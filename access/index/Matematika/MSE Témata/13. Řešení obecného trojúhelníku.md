1. Řešte $\triangle ABC$ a určete jeho obsah, je-li dáno:

   - $c=10\ cm$, $\alpha=45\textdegree$, $\beta=105\textdegree$

   > [!done]- Řešení
   >
   > $$
   > \begin{aligned}
   >   \gamma &= 30\textdegree \\
   >   \\
   >   \frac{a}{\sin\left(\alpha\right)} &= \frac{b}{\sin\left(\beta\right)} &= \frac{c}{\sin\left(\gamma\right)} \\
   >   \frac{a}{\frac{1}{\sqrt{2}}} &= \frac{b}{\sin\left(\beta\right)} &= \frac{10cm}{\frac{1}{2}} \\
   >   a\sqrt{2} &= \frac{b}{\sin\left(\beta\right)} &= 20cm \\
   >   \\
   >   &\begin{cases}
   >     a\sqrt{2} &= 20cm \\
   >     \frac{b}{\sin\left(\beta\right)} &= 20cm
   >   \end{cases} \\
   >   &\begin{cases}
   >     a &= \frac{20}{\sqrt{2}}cm \\
   >     b &= \sin\left(105\textdegree\right)\times20cm
   >   \end{cases} \\
   >   \\
   >   s &= \frac{a+b+c}{2} \\
   >   A &= \sqrt{s\left(s-a\right)\left(s-b\right)\left(s-c\right)} \\
   >   A &\approx 68.30127019cm^{2}
   > \end{aligned}
   > $$

   - $a=18\ cm$, $b=11\ cm$, $\alpha=78\textdegree$

   > [!todo]- Řešení

2. V $\triangle ABC$ je dáno $a=14\ cm$, $b=9\ cm$, $c=16\ cm$.

   - Vypočítejte délku výšky $v_c$ a těžnice $t_c$.

   > [!todo]- Řešení

3. Na těleso působí síly $F_1=80N$ a $F_2=50N$, které spolu svírají úhel $55\textdegree$.

   - Určete velikost výsledné síly a úhel, který výslednice svírá se silou $F_1$.

   > [!todo]- Řešení

4. Délky stran $\triangle XYZ$ jsou v poměru $x:y:z=3:5:7$.

   - Vypočítejte velikosti vnitřních úhlů.

   > [!todo]- Řešení

5. Pozorovatel vidí patu věže vysoké $68\ m$ v hloubkovém úhlu $15\textdegree$ a vrchol věže ve výškovém úhlu $30\textdegree$.

   - Jak vysoko je pozorovatelovo stanoviště nad vodorovnou rovinou, na níž stojí věž?

   > [!todo]- Řešení

6. Vrchol věže stojící na vodorovné rovině vidíme z místa $A$ pod výškovým úhlem $\alpha=35\textdegree$.
   Přijdeme-li o $16\ m$ blíž k věži na místo $B$, uvidíme vrchol pod výškovým úhlem $\beta=42\textdegree$.

   - Jak vysoká je tato věž?

   > [!todo]- Řešení
