#!/bin/bash

cd "$(dirname "$0")"

function convert_latex_to_png {
  echo "Converting '$1.latex' to png..."
  pdflatex -interaction batchmode "$1.latex" && sleep 0.1
  convert -background white -alpha remove -alpha off -density 300 "$1.pdf" "$1.png"
}

if [[ -z "$1" ]]; then
  for f in *.latex; do
    convert_latex_to_png "$(basename $f .latex)"
  done
else
  convert_latex_to_png "$1"
fi
