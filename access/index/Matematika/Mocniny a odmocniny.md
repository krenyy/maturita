$$
  \Large
  \begin{aligned}
    a^{n}\times a^{m} &= a^{n+m} \\
    \\
    \left(a^{n}\right)^{m} &= a^{n\times m} \\
    \\
    \sqrt[a]{x} &= x^{\frac{1}{a}} \\
    \\
    \sqrt[a]{x\times y} &= \sqrt[a]{x}\times\sqrt[a]{y} \\
    \\
    \sqrt[a]{x}^{y} &= x^{\frac{y}{a}}
  \end{aligned}
$$
