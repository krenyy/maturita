$$
\Large \begin{aligned} log_a\left(x\times y\right) &= log_a\left(x\right)+log_a\left(y\right) \\\\ log_a\left(\frac{x}{y}\right) &=
log_a\left(x\right)-log_a\left(y\right) \\\\ log_a\left(x^{n}\right) &= n\times log_a\left(x\right) \end{aligned}
$$
